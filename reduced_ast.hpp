#include <vector>
#include <iostream>
#include <cinttypes>
#include <map>
#include <algorithm>
#include <string>
using namespace std;

void print_types(int type);
class Op{
public:
	int length;
	int count;
	vector<Op> args;
	unsigned char type;
	string name;
	/*
		0:or
		1:and
		2:not
		2+:vars(letter depend of type number)
	*/
	Op(int type);
	Op(char type);
	Op(unsigned char type);
	Op(int length, int count, vector<Op> args, int type, string name);
	Op(unsigned char type, string name);
	//Op(const Op& t);
	Op();
	void addElement(Op element);
	void show();
	void addStart(Op element);
	int is_many_occurences(Op expr, int maxi, int s);
	char operator==(const Op other) const;
	char operator<(const Op other) const;
	char operator>(const Op other) const;
	Op operator~() const;
	Op operator|(const Op other) const;
	Op operator&(const Op other) const;
	int subs(Op bef, Op aft);
//	void prepare();
};
Op createOp(int type, vector<Op> v, vector<Op>::iterator end);
int get_(Op code, vector<Op> out, int* lenmax);
bool cmp(pair<Op, int>& a, 
		 pair<Op, int>& b); 
 
// Function to sort the map according 
// to value in a (key-value) pairs 
vector<pair<Op, int>> sortMap(map<Op, int>& M);
int optimize(Op code);
