#include "reduced_ast.hpp"

void print_types(int type){
	switch(type){
		case 0:
			cout << " | ";
			break;
		case 1:
			cout << " & ";
			break;
		case 3:
			cout << " = ";
			break;
		case 4:
			cout << endl;
			break;
	}
}
//class Op{
//public:
/*int length;
int count;
vector<Op> args;
unsigned char type;
string name;
/*
	0:or
	1:and
	2:not
	2+:vars(letter depend of type name)
*/
char cmp_Op(const Op& op1, const Op& op2){
	return op1>op2;
}
Op::Op(int type):type(type){length=0;count=0;name="";}
Op::Op(char type):type(type){length=0;count=0;name="";}
Op::Op(unsigned char type):type(type){length=0;count=0;name="";}
//Op::Op(int length, int count, vector<Op> args, int type, string name):length(length), count(count), args(args), type(type), name(name){}
Op::Op(unsigned char type, string name):type(type), name(name){length=0;count=0;}
/*Op::Op(const Op& t){
	length = t.length;
	count = t.count;
	args = t.args;
	type = t.type;
	name = t.name;
}*/
Op::Op(){
	length = 0;
	//cout << length;
	count = 0;
	type = 5;
	name = "";
}
Op Op::operator~() const{
	Op res(2);
	res.addElement(*this);
	return res;
}
Op Op::operator|(const Op other) const{
	Op res(0);
	if(other.type == type && type == 0){
		res.args = other.args;
		res.args.insert(res.args.end(), args.begin(), args.end());
		res.length = other.length+length;
		res.count = other.count+count;
		sort(res.args.begin(), res.args.end(), cmp_Op);
	}else if(other.type == 0){
		res.args = other.args;
		res.length = other.length;
		res.count = other.count;
		res.addElement(*this);
	}else if(type == 0){
		res.args = args;
		res.length = length;
		res.count = count;
		res.addElement(other);
	}else{
		res.addElement(other);
		res.addElement(*this);
	}
	//cout << res.length;
	return res;
}
Op Op::operator&(const Op other) const{
	Op res(1);
	if(other.type == type && type == 1){
		res.args = other.args;
		res.args.insert(res.args.end(), args.begin(), args.end());
		res.length = other.length+length;
		res.count = other.count+count;
		sort(res.args.begin(), res.args.end(), cmp_Op);
	}else if(other.type == 1){
		res.args = other.args;
		res.length = other.length;
		res.count = other.count;
		res.addElement(*this);
	}else if(type == 1){
		res.args = args;
		res.length = length;
		res.count = count;
		res.addElement(other);
	}else{
		res.addElement(other);
		res.addElement(*this);
	}
	//cout << res.length;
	return res;
}
//char Op::operator!=(const Op other) const{}
char Op::operator==(const Op other) const{
	if(other.type == type && length == other.length){
		if(type == 5)
			return name == other.name;
		for(int i=0; i<length; i++){
			if(!(other.args[i] == args[i])){
				return 0;
			}
		}
		return 1;
	}
	return 0;
	//return other.type == type && (type != 5 || name == other.name) && (length == other.length) && equal(other.args.begin(), other.args.begin()+length, args.begin());
}
/*char operator==(const Op& from, Op other){
	return other.type == from.type && from.length == other.length && equal(other.args.begin(), other.args.begin()+other.length, from.args.begin());
}*/
char Op::operator<(const Op other) const{
	if(other.type == type){
		if(type == 5)return name<other.name;
		if(length < other.length){
			for(int i=0; i<length; i++){
				if(args[i] < other.args[i]){
					return true;
				}
				if(args[i] > other.args[i]){
					return false;
				}
			}
			return true;
		}else{
			for(int i=0; i<other.length; i++){
				if(args[i] < other.args[i]){
					return true;
				}
				if(args[i] > other.args[i]){
					return false;
				}
			}
			return false;
		}
	}
	else return type < other.type;
}
char Op::operator>(const Op other) const{
	if(other.type == type){
		if(type == 5)return name>other.name;
		if(length < other.length){
			for(int i=0; i<length; i++){
				if(args[i] > other.args[i]){
					return true;
				}
				if(args[i] < other.args[i]){
					return false;
				}
			}
			return false;
		}else{
			for(int i=0; i<other.length; i++){
				if(args[i] > other.args[i]){
					return true;
				}
				if(args[i] < other.args[i]){
					return false;
				}
			}
			return true;
		}
	}
	else return type > other.type;
}
void Op::addElement(Op element){
	args.push_back(element);
	if(type != 4){
		if(length || type == 2){
			count++;
		}
		sort(args.begin(), args.end(), cmp_Op);
	}
	count += element.count;
	/*if(length > 100){
		cout << length << " " << 160 << endl;
		exit(1);
	}*/
	length++;
}
void Op::show(){
	if(length){
		if(type == 2){
			cout << "~";
			args[0].show();
			return;
		}
		args[0].show();
		for(int i=1; i<length; i++){
			print_types(type);
			args[i].show();
		}
	}else if(type == 5){
		cout << name;
		return;
	}
}
void Op::addStart(Op element){
	args.insert(args.begin(), element);
	length++;
	if(type != 4){
		if(length || type == 2)
			count++;
	}
	count += element.count;
}
int Op::is_many_occurences(Op expr, int maxi, int s){
	if(expr.type == type){
		int j = -1;
		int lenS = length-expr.length+1;
		bool all_correct=true;
		for(int i=0; i<expr.length; i++){
			bool found=true;
			for(j++; j<lenS; j++){
				if(args[j] == expr.args[i]){
					found=false;
					break;
				}
			}
			if(found){
				all_correct=false;
				break;
			}
			lenS++;
		}
		if(all_correct)
			s++;
	}
	for(int i=0; i<length; i++){
		if(s > maxi)
			return s;
		else{
			if(args[i] == expr){
				s++;
				//show();cout << endl;
			}else{
				s = args[i].is_many_occurences(expr, maxi, s);
			}
		}
	}
	return s;
}
int Op::subs(Op bef, Op aft){
	int s = 0;
	if(type == 5)return 0;
	if(type == bef.type && type != 2){
		vector<Op> newargs;
		newargs.push_back(aft);
		int starting=0;
		int end = length-bef.length+1;
		bool all_found=true;
		int j;
		for(int i=0; i<bef.length; i++){
			//if starting >= end:break
			bool found = true;
			for(j=starting; j<end; j++){
				if(args[j] == bef.args[i]){
					found=false;
					break;
				}
			}
			if(found){
				all_found=false;
				break;
			}else{
				newargs.insert(newargs.begin(), args.begin()+starting, args.begin()+j);
				//newargs.extend(args[starting:j]);
				starting = j+1;
				end++;
			}
		}
		if(all_found){
			if(starting != end)
				newargs.insert(newargs.begin(), args.begin()+starting, args.end());
			args = newargs;
			s = 1;
		}
	}
	length = args.size();
	if(type != 4){
		count = length-1;
		if(count==0)count=1;
	}else count = 0;
	for(int i=0; i<length; i++){
		if(args[i].type == 5)continue;
		if(args[i] == bef){
			s++;
			/*show();
			cout << " ------ ";
			bef.show();
			cout << endl;*/
			args[i] = aft;
			count += aft.count;
		}else{
			/*args[i].show();
			cout << " " << int(args[i].args[0].name == bef.args[0].name) << " " << int(args[i].type == bef.type) << " " << args[i].args.size() << " " << bef.args.size() << endl;
			show();
			cout << " ====== ";
			bef.show();
			cout << endl;*/
			s += args[i].subs(bef, aft);
			count += args[i].count;
		}
	}
	if(s && type != 4){
		/*show();
		cout << " ++++++ ";
		bef.show();
		cout << endl;*/
		sort(args.begin(), args.end(), cmp_Op);
	}
	/*if s:
		if len(self.args) > 1:
			self.h = tuple(self.args).__hash__()^H[self.Tstr]
		else:
			self.h = self.args[0].h^H[self.Tstr]
		self.args.sort()*/
	return s;
}
//	void prepare();
//};
Op createOp(int type, vector<Op> v, int length){
	Op res(type);
	//cout << "1\n";
	for(int i=0; i<length; i++){
		//cout << "2\n";
		res.addElement(v[i]);
	}
	//cout << "3\n";
	//cout << res.length;
	return res;
}
int get_(Op code, vector<Op>* out, int* lenmax){
	int s = 0;
	/*cout << "test" << out.size() << endl;
	code.show();
	cout << " " << (int)code.type << endl;*/
	if(code.type == 3){
		return get_(code.args[1], out, lenmax);
	}else if(code.type > 4)
		return 0;
	else if(code.type < 4){
		out->push_back(code);
		//cout << "-" << out.size() << endl;;
		if(*lenmax < code.length){
			//cout << code.length;
			*lenmax = code.length;
		}
		s = 1;
	}
	for(int i=0; i<code.length; i++){
		/*if(code.args[i].type == 2 && code.args[i].length != 1){
			cout << "!=!=!=!=!=!=!=!=!=!=\n";
			for(int j=0; j<code.args[i].length; j++){
				code.args[i].args[j].show();
				cout << " ";
			}
			cout << endl;
		}*/
		s += get_(code.args[i], out, lenmax);
	}
	return s;
}
bool cmp(pair<Op, int>& a, 
		 pair<Op, int>& b){ 
	return a.second*a.first.count < b.second*b.first.count; 
} 
 
// Function to sort the map according 
// to value in a (key-value) pairs 
vector<pair<Op, int>> sortMap(map<Op, int>& M){ 
 
	// Declare vector of pairs 
	vector<pair<Op, int> > A; 

	// Copy key-value pair from Map 
	// to vector of pairs 
	for (auto& it : M){ 
		A.push_back(it); 
	} 

	// Sort using comparator function 
	sort(A.begin(), A.end(), cmp);
	return A;
} 
int optimize(Op* code){
	//cout << "1\n";
	map<Op, int> commons;
	vector<Op> commonsKey;
	//cout << "2\n";
	int lencommons = 0;
	vector<Op> ops;
	int lenmax=0;
	//cout << "3\n";
	int Len = get_(*code, &ops, &lenmax);
	//cout << lenmax << " 4\n";
	//cout << Len << " " << ops.size() << "\n";
	//vector<Op> v(lenmax*2); 
	//vector<Op>::iterator it, st; 
	//cout << "5\n";
	for(int i=0; i<Len; i++){
		for(int j=i+1; j<Len; j++){
			//cout << i << " " << j << endl;
			if(ops[i].type != ops[j].type)continue;
			vector<Op> dest;
			int length = 0;
			for (std::vector<Op>::iterator x = ops[i].args.begin(); x != ops[i].args.end(); ++x){
				if (std::find(ops[j].args.begin(), ops[j].args.end(), *x) != ops[j].args.end())
				{
					dest.push_back(*x);
					length++;
				}
			}
			//it = set_intersection(ops[i].args.begin(), ops[i].args.end(), ops[j].args.begin(), ops[j].args.end(), v.begin());
			//int length = it-v.begin();
			//ops[i].show();cout << endl;
			//ops[j].show();cout << endl;
			//cout << endl;
			if(length > 1 || (ops[i].type == 2 && length)){
				//cout << length << " " << v.size() << " 1\n";
				Op sr = createOp(ops[i].type, dest, length);
				//cout << "2\n";
				//sr.show();
				if(commons.find(sr) != commons.end()){
					commons[sr]+= 2;
				}else{
					commons[sr] = 2;
					//sr.show();
					//cout << endl;
					lencommons++;
					commonsKey.push_back(sr);
				}
				//cout << "end\n";
			}
		}
	}
	//cout << "6\n";
	vector<pair<Op, int>> L = sortMap(commons);
	//cout << L.size() << " " << lencommons << " 7\n";
	bool* exclude = (bool*)calloc(sizeof(bool), lencommons);
	while(1){
		//cout << " 1\n";
		vector<pair<Op, int>> newL(lencommons);// = calloc(sizeof(Op), lencommons);
		for(int i=0; i<lencommons; i++){
			exclude[i] = false;
		}
		//cout << " 2\n";
		bool sec = true;
		int index = 0;
		for(int i=0; i<lencommons; i++){
			if(exclude[i])continue;
			for(int j=i+1; j<lencommons; j++){
				if(!exclude[j] && L[j].first.is_many_occurences(L[i].first, 0, 0)){
					//L[i].first.show();
					//cout << " eq ";
					//L[j].first.show();cout << endl;
					newL[index++] = L[j];
					exclude[j] = true;
					sec = false;
				}
			}
			newL[index++] = L[i];
		}
		//cout << " 3\n";
		bool cont=true;
		for(int i=0; i<lencommons; i++){
			if(L[i] != newL[i]){
				cont=false;
				break;
			}
		}
		//cout << " 4\n";
		if(cont)
			break;
		for(int i=0; i<lencommons; i++){
			L[i] = newL[i];
			//L[i].first.show();
			//cout << endl;
		}
		//cout << " 5\n";
		if(sec)
			break;
	}
	//cout << "8\n";
	int s = 0;
	uint8_t varR = 0;//82 = "R"
	string base = "R";
	for(int i=0; i<lencommons; i++){
		int t=code->is_many_occurences(L[i].first, 1, 0);
		//L[i].first.show();
		//cout << " " << t << endl;
		if(t > 1){
			//L[i].first.show();
			//cout << endl;
			//cout << "-----\n";
			Op R(5, base+to_string(varR));
			s += code->subs(L[i].first, R);
			//cout << s << endl;
			Op A(3);
			A.addElement(R);
			A.addElement(L[i].first);
			//A.prepare();
			code->addStart(A);
			varR++;
			//cout << "=============================\n";
			//code->show();
			//cout << endl;
		}
	}
	return s;
}

int main(){
	Op all(4);
	Op a(5, (string)"a");
	Op b(5, (string)"b");
	Op c(5, (string)"c");
	Op d(5, (string)"d");
	Op e(5, (string)"e");
	Op f(5, (string)"f");
	Op ass1(3);
	Op ass2(3);
	ass1.addElement(a);
	ass1.addElement((~f & c) | (e & c) | ~d);
	ass2.addElement(b);
	ass2.addElement((~f & c & ~d) | (~c & ~e & f) | (e & ~f) | (~e & f & d));
	all.addElement(ass1);
	all.addElement(ass2);
	all.show();
	cout << endl;
	cout << "finit\n";
	cout << optimize(&all) << endl;
	all.show();
	cout << endl << all.count << endl;
	/*c = Block(
	Assignment(a, (~f & c) | (e & c) | ~d),
	Assignment(b, (~f & c & ~d) | (~c & ~e & f) | (e & ~f) | (~e & f & d))
	)
	print(c)
	print(optimize(c))
	print(c)*/
}
