#include "binary.cpp"
#include "reduced_ast.cpp"
//from logicmin.reduced_ast import true, false, Or, And, Var, Not
int cubes_cost(Cube* Cubes, int gc, int tc, int mypc){
	//mypc = len(Cubes)
	int cpc = 0;
	for(int i=0; i<mypc: i++){
		cpc += Cubes[i].cost(gc, tc);
	}
	if(mypc > 1)
		return cpc + (mypc + 1) * tc + gc; // add PCs of the or gate;
	else if(mypc == 1):
		return cpc;  // no or gate
	else:
		return 0;
}

Cube minterm_to_cube(int MAX_VARS, int minterm){
	int t = 0;
	int f = 0;
	int mask = 1;
//	print "minterm=",minterm
	for(int b=0; b<MAX_VARS; b++){
		if((minterm & mask) > 0){
			t |= mask;
		}else
			f |= mask;
		mask <<= 1;
	}
	return Cube( MAX_VARS, t, f )
}

int num_to_cube (int num, int MAX_VARS){
	return minterm_to_cube(MAX_VARS,num);
}

bool oneone (int w, int MAX_VARS){
	return __builtin_popcount(w) == 1;
}
bool combinable (Cube C1, Cube C2){
	int twordt = C1.t ^ C2.t;
	return (C1.xor_ == C2.xor_) && !(twordt&(twordt-1));
}

Cube combine (Cube C1, Cube C2):
	return Cube( C1.MAX_VARS, C1.t & C2.t, C1.f & C2.f);

/*Cube CombToCube( comb, MAX_VARS ):
	vs = list(comb)
	vs.reverse()
	int t = 0;
	int f = 0;
	int pwr = 1;
	for i in range(MAX_VARS):
		c = vs[i];
		if(c=='1')
			t |= pwr; 
		else if(c=='0')
			f |= pwr;
		pwr <<= 1;
	return Cube(MAX_VARS,t,f);

def CubeExpandRHS( comb, MAX_VARS ):
	return CombToCube(comb,MAX_VARS)*/

def CubeExpandLHS( comb, MAX_VARS ):
	vs = list(reversed(comb))
	i = 0
	pos = []
	for c in vs:
		if (c=='-'):
			pos.append(i);
		i += 1
	
	cnt = len(pos)
	n = 1<<cnt
#	print "n=",n," cnt=",cnt," pos=",pos
	cubes = []
	xs = vs
	xb0 = str_to_binary(xs,MAX_VARS)
#	print " xs=",xs," xb0=",xb0
	for w in range(n):
		bb = binary(w,cnt)
		xb = list(xb0)
		for i in range(cnt):
			xb[pos[i]] = bb[i]
#		print " xb=",xb
		minterm = numeric(xb,MAX_VARS)
		cube = minterm_to_cube(MAX_VARS,minterm)
		cubes.append(cube)
	return cubes	

Cube::Cube(int MAX_VARS, int t, int f);:MAX_VARS(MAX_VARS), t(t), f(f){
	xor_ = t^f;
}
Cube::Cube(int MAX_VARS):MAX_VARS(MAX_VARS){
	t=0, f=0, xor_=0;
}

bool Cube::operator==(Cube other){
	return self.t == other.t && self.f == other.f;
}
		
	def __hash__(self):
		return pow(self.t, self.f, self.xor)

void reverse(Op* table, int size){

    Op* start = table;
    Op* end = table + (size - 1);

    for (int i = 0; i < (size>>1); i++){

        Op temp = *end;
        *end = *start;
        *start = temp;

        start++;
        end--;
    }
}
Op Cube::ast(Op* names, bool sum_, int lnames):
	Op out((int)(!sum_));
	int t_ = t
	int f_ = f
	for(int b=0; b<MAX_VARS; b++){
		Op name(5);
		if(b<lname){
			name.name = names[b].name;
		}else{
			name.name = ((string)"X")+to_string(b)
		}
		if(t&1){
			out.addElement(name);
		}else if(f&1){
			out.addElement(~name);
		t >>= 1;
		f >>= 1;
	}
	if(out.length==0){
		if(sum_){
			return False;
		}else{
			return True;
		}
	}else if(out.length == 1){
		return out.args[0];
	}
	return out;

	def covers( self, minterm ):
		mask = self.t | self.f
		return (minterm & mask == self.t) and \
			   ((~minterm) & mask == self.f)

	def cover_set( self, minterms):
		return filter(self.covers, minterms)


	def cost( self ):
		return countones(self.t,self.MAX_VARS) + \
			   countones(self.f,self.MAX_VARS)

	def input_pin_cnt( self ):
		return countones(self.t,self.MAX_VARS) + \
			   countones(self.f,self.MAX_VARS)

	def pc( self,gc=1,tc=1 ):
		cnt = self.input_pin_cnt();
		if cnt > 1:
			return tc * (cnt + 1) + gc
		elif cnt == 1:
			return 0 # it is a wire
		else:
			return 0

	def cost( self, gc=1, tc=1 ):
		cnt = self.input_pin_cnt();
		if cnt > 1:
			return tc * (cnt + 1) + gc
		elif cnt == 1:
			return 0 # it is a wire
		else:
			return 0


	def invert( self ):
		self.t, self.f = self.f, self.t



	def Value( self ):
		mask = 1 << self.MAX_VARS
		diff = (self.t - (~self.f)) & (~mask)
		tt = diff == 0
		if (tt):
			return self.t
		else:
			return None

	# 0, 1, (dc=-1)
	def BitValue( self, bit ):
		mask = 1 << bit
		mt = mask & self.t
		mf = mask & self.f
		if (mt and not mf):
			return 1
		if (mf and not mt):
			return 0
		return -1
		
	def Expand(self):
		xor = (self.t ^ self.f)
		tt = ~xor
		if (tt==0):
			return [self]
		return None

	def Concat(self,y):
		c = Cube(self.MAX_VARS + y.MAX_VARS)
		c.t = self.t << y.MAX_VARS | y.t
		c.f = self.f << y.MAX_VARS | y.f
		return c

	def Copy(self):
		c = Cube(self.MAX_VARS,self.t,self.f)
		return c
		
