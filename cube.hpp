#include "reduced_ast.cpp"
class Cube{
public:
	int t, f, xor_;
	int MAX_VARS;
	Cube(int MAX_VARS, int t, int f);
	Cube(int MAX_VARS);
	bool operator==(Cube other);
	Op ast(Op* names, bool sum_);
	bool covers(int minterms);
	int cover_set(int minterms);
	int cost(void);
	int input_pin_cnt(void);
	int pc(int gc, int tc);
	int cost(int gc, int tc);
	void invert(void);
	
}
